# GitlabCI snippets

Those are snippets that you can include in your own `.gitlab-ci.yml` (see [documentation](https://docs.gitlab.com/ce/ci/yaml/README.html#include-examples)).

# Snippets

- `docker-it-from-ci.gitlab-ci.yml`: on commit (on a given branch), build the project Dockerfile, and push it to a given repository, tagged with the commit hash. Likewise on tag, but with the tag as a docker image tag.
- `pouet-it-from-ci.gitlab-ci.yml`: on tag, publish a message on [Mastodon](https://joinmastodon.org)
- `publish_changelog.gitlab-ci.yml`: on tag, publish the content of your CHANGELOG as release informations on the gitlab page corresponding to your tag.

# License

© 2019 Luc Didry, released under the terms of the GNU GPL v3. See the [LICENSE](LICENSE) file.
